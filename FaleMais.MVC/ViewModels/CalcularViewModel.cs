﻿namespace FaleMais.MVC.ViewModels
{
    public class CalcularViewModel
    {
        public int Origem { get; set; }
        public int Destino { get; set; }
        public int PlanoId { get; set; }
        public int Tempo { get; set; }
    }
}